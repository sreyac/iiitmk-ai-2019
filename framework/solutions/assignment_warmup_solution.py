def test(solution):
    def reference(n):
        return [i * 2 for i in range(n)]

    for n in [0, 1, 10, 100, 1000]:
        assert list(solution(n)) == list(reference(n)), f"Your solution fails for n={n}"
